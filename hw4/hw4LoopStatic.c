#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

void doWork(int t) {
   sleep(t);
}

int* initWork(int n) {
   int i;
   double r;
   int* wA = (int *) malloc(sizeof(int)*n);   
   for (i = 0; i < n; i++) {
      wA[i] = rand( )%2*i/(n/10);
   }
   return wA;
}

int main (int argc, char *argv[]) {
   int i;
   int *w = initWork(10000);
   double t1 = omp_get_wtime();
#pragma omp parallel for schedule(static)
   for (i = 0; i < 10000; i+=500) {
      printf("w[%d] = %d\n", i, w[i]);
      doWork(w[i]);
   }
   double t2 = omp_get_wtime();
   printf("Total time 1 %.8f\n", t2-t1);
   
   t1 = omp_get_wtime();
#pragma omp parallel for schedule(static, 50)
   for (i = 0; i < 10000; i+=500) {
      printf("w[%d] = %d\n", i, w[i]);
      doWork(w[i]);
   }
   t2 = omp_get_wtime();
   printf("Total time 2 %.8f\n", t2-t1);
   t1 = omp_get_wtime();
#pragma omp parallel for schedule(dynamic)
   for (i = 0; i < 10000; i+=500) {
      printf("w[%d] = %d\n", i, w[i]);
      doWork(w[i]);
   }
   t1 = omp_get_wtime();
   t2 = omp_get_wtime();
   printf("Total time 3 %.8f\n", t2-t1);
#pragma omp parallel for schedule(dynamic, 50)
   for (i = 0; i < 10000; i+=500) {
      printf("w[%d] = %d\n", i, w[i]);
      doWork(w[i]);
   }
   t2 = omp_get_wtime();
   printf("Total time 4 %.8f\n", t2-t1);
}

