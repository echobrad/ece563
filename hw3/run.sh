gcc -fopenmp -o hw3a hw3a.c
./hw3a
gcc -fopenmp -o hw3b hw3b.c
./hw3b
gcc -fopenmp -o hw3c hw3c.c
./hw3c

# work 2
gcc -fopenmp -o second second.c
./second

# work 3
gcc -fopenmp -o third third.c
./third

# work 4
gcc -fopenmp -o fourth fourth.c
./fourth
