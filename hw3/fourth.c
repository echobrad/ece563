#include <omp.h>
#include <stdio.h>

int main() {
	int i;
	#pragma omp parallel
	for (i = 0; i < 10; i++) {
		printf("parallel loop: %d\n", i);
	}
	#pragma omp parallel for
	for (i = 0; i < 10; i++) {
		printf("parallel for loop: %d\n", i);
	}
	return 0;
}
