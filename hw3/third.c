#include <omp.h>
#include <stdio.h>

int main() {
	int id;
	#pragma omp parallel sections
	{
		#pragma omp section
			printf("section 1\n");
		#pragma omp section
			printf("section 2\n");
	}
	return 0;
}
