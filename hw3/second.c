#include <omp.h>
#include <stdio.h>

int main() {
	int id;
	#pragma omp parallel
	{
		#pragma omp master
		{
			id = omp_get_thread_num();	
			printf("master thread id: %d\n", id);
		}
		#pragma omp single
		{
			id = omp_get_thread_num();	
			printf("single thread id: %d\n", id);
		}
	
		#pragma omp critical
		{
			id = omp_get_thread_num();
			printf("critical thread id: %d\n", id);
		}
	
	}
	return 0;
}
