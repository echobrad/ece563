#include <omp.h>
#include <stdio.h>

int main() {
	int id;
	int num = omp_get_num_procs();
	printf("%d num of procs!\n", num);
	#pragma omp parallel private(id) 
	{
		id = omp_get_thread_num();
		printf("%d: Hello world!\n", id);
	}
	return 0;
}
