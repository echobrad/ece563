#default num of threads is num of cores
gcc -fopenmp -o matrix_multi matrix_multi.c

export OMP_NUM_THREADS=2
./matrix_multi

export OMP_NUM_THREADS=4
./matrix_multi
