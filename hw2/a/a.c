#include <omp.h>
#include <stdio.h>

int main() {
	int num = omp_get_num_procs();
	printf("%d num of procs!\n", num);
	
	int id;
	#pragma omp parallel private(id)
	{
		id = omp_get_thread_num();
			printf("thread id: %d\n", id);
	}
	#pragma omp master
	{
		id = omp_get_thread_num();
		
		printf("master thread id: %d\n", id);
	}
	#pragma omp single private(id)
	{
		id = omp_get_thread_num();
		
		printf("single thread id: %d\n", id);
	}
	return 0;
}
