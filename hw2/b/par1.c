#include <omp.h>
#include <stdio.h>

int main() {
	double arr[1000000];
	int i;
	int sum;

	for (i = 0; i < 1000000; i++)
		arr[i] = i;

	int num = omp_get_max_threads();
	int res[num*8];
	int id;
	double t1 = omp_get_wtime();
	#pragma omp parallel for private(id)
	for (i = 0; i < 1000000; i++)
	{
		id = omp_get_thread_num();
		res[id*8] += arr[i];
	}
	double t2 = omp_get_wtime();
	printf("Total time %.8f\n", t2-t1);
	return 0;
}
