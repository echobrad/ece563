#include <omp.h>
#include <stdio.h>

int main() {
	double arr[1000000];
	int i;
	double sum;

	for (i = 0; i < 1000000; i++)
		arr[i] = 1.0/i;

	double t1 = omp_get_wtime();
	for (i = 0; i < 1000000; i++)
		sum += arr[i];
	
	double t2 = omp_get_wtime();
	printf("Total time %.8f\n", t2-t1);
	return 0;
}
