#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
	double *arr = malloc(10000000 * sizeof(double));
	int i;

	for (i = 0; i < 10000000; i++)
		arr[i] = 1.0/i;

	double sum = 0;
	double t1 = omp_get_wtime();
	#pragma omp parallel for reduction(+:sum)
		for (i = 0; i < 10000000; i++)
			sum = sum + arr[i];
	double t2 = omp_get_wtime();
	printf("Total time %.8f\n", t2-t1);
	free(arr);
	return 0;
}
