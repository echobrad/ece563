#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MIN_PER_RANK 1 /* Nodes/Rank: How 'fat' the DAG should be.  */
#define MAX_PER_RANK 20
#define MIN_RANKS 3    /* Ranks: How 'tall' the DAG should be.  */
#define MAX_RANKS 20
#define PERCENT 20     /* Chance of having an Edge.  */
#define SIZE 80     /* Total number of nodes in the graph */

int main (void)
{
  int i, j, k, nodes = 0;
  //srand (time (NULL));
  srand (101);
  int graph[SIZE][SIZE]; /* If graph[i][j] = 1, there is an edge from */
                         /* node i to node j in the graph. */

  for (i = 0; i < SIZE; i++) 
     for (j = 0; j < SIZE; j++) 
        graph[i][j] = 0;

  int ranks = MIN_RANKS
              + (rand () % (MAX_RANKS - MIN_RANKS + 1));

  printf ("digraph {\n");
  for (i = 0; i < ranks; i++)
    {
      /* New nodes of 'higher' rank than all nodes generated till now.  */
      int new_nodes = MIN_PER_RANK
                      + (rand () % (MAX_PER_RANK - MIN_PER_RANK + 1));

      /* Edges from old nodes ('nodes') to new ones ('new_nodes').  */
      for (j = 0; j < nodes; j++)
        for (k = 0; k < new_nodes; k++)
          if ( ((rand () % 100) < PERCENT)) //  && ((k+nodes) < SIZE))
            if ((j < SIZE) && (k+nodes < SIZE))
               {
               printf ("  %d -> %d;\n", j, k + nodes); /* An Edge.  */
               graph[j][k+nodes] = 1;
               }
      nodes += new_nodes; /* Accumulate into old node set.  */
    }
  printf ("}\n");
  for (i = 0; i < SIZE; i++) 
    {
     for (j = 0; j < SIZE; j++) 
        printf("%d ", graph[i][j]);
     printf("\n");
    }

  return 0;
}
