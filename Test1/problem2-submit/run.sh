gcc -fopenmp -o graphCount-par graphCount-par.c
./graphCount-par > par_result
gcc -fopenmp -o graphCount-for graphCount-for.c
./graphCount-for > for_result
rm graphCount-par
rm graphCount-for
